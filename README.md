# Kenny Webhook
Pratice webhook on Docker

## How to build
`$ docker build -t <your username>/kenny_webhook .`

## How to run
`$ docker run -p 49160:8080 -d <your username>/kenny_webhook`

## How to test
`$ curl -i localhost:49160`

`$ curl -X GET "localhost:49160/webhook?hub.verify_token=YOUR_VERIFY_TOKEN&hub.challenge=CHALLENGE_ACCEPTED&hub.mode=subscribe"`

`$ curl -H "Content-Type: application/json" -X POST "localhost:49160/webhook" -d '{"object": "page", "entry": [{"messaging": [{"message": "TEST_MESSAGE"}]}]}'`

### References
* [Dockerizing a Node.js web app](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)
* [Webhook 설정](https://developers.facebook.com/docs/messenger-platform/getting-started/webhook-setup)
